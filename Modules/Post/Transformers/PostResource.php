<?php

namespace Modules\Post\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);


        $images = [];
        foreach ($data['images'] as $item) {
            $images[] = [
                'id' => $item['id'],
                'url' => config('app.url') . '/' . $item['url']
            ];
        }

        return [
            'id' => $data['id'],
            'title' => $data['title'],
            'subtitle' => $data['subtitle'],
            'text' => $data['text'],
            'likes' => array_key_exists('like', $data) ? $data['likes'] : 0,
            'views' => array_key_exists('view', $data) ? $data['view'] : 0,
            'created_at' => implode(' - ', toJalali($data['created_at'])),
            'images' => $images
        ];
    }
}
