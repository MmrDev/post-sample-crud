<?php

namespace Modules\Post\Http\Controllers\Api;

use App\Traits\Response;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Post\Http\Requests\CreatePostRequest;
use Modules\Post\Http\Requests\UpdatePostRequest;
use Modules\Post\Repositories\PostRepositoryInterface;
use Modules\Post\Transformers\PostResource;

class PostController extends Controller
{

    use Response;
    private $repository;

    public function __construct(PostRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }


    public function store(CreatePostRequest $request)
    {

        $postInstance = $this->repository->createPost($request->validated());
        if ($postInstance) {
            return $this->successResponse(200, new PostResource($postInstance), 200);
        }
        return $this->errorResponse(400, __('errors.noData'), 400);
    }


    public function index()
    {
        $postCollection = $this->repository->getAll();
        if ($postCollection) {
            $data = $postCollection->toArray();
            $data['data'] = PostResource::collection($data['data']);
            return $this->successResponse(200, $data, 200);
        }
        return $this->errorResponse(400, __('errors.noData'), 400);
    }


    public function show($postId)
    {
        $postInstance = $this->repository->getPost($postId);
        if ($postInstance) {
            return $this->successResponse(200, new PostResource($postInstance), 200);
        }
        return $this->errorResponse(400, __('errors.noData'), 400);
    }


    public function update($postId, UpdatePostRequest $request)
    {
        $data = $request->validated();


        // put images on data
        if ($request->has('images')) {
            foreach ($request->images as $key => $item) {
                $data['images'][] = [
                    'id' => $key,
                    'image' => $item
                ];
            }
        }

        $postInstance =  $this->repository->updatePost($postId, $data);
        if ($postInstance) {
            return $this->successResponse(200, new PostResource($postInstance), 200);
        }
        return $this->errorResponse(400, __('errors.noData'), 400);
    }


    public function destroy($postId)
    {
        $postInstance = $this->repository->DestroyPost($postId);
        if ($postInstance) {
            return $this->successResponse(200, __('messages.deleted_successfully'), 200);
        }
        return $this->errorResponse(400, __('errors.noData'), 400);
    }
}
