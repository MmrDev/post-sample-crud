<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PostImage extends Model
{
    use HasFactory;

    protected $fillable = [
        'post_id',
        'url',
        'active',
    ];

    protected static function newFactory()
    {
        return \Modules\Post\Database\factories\PostImageFactory::new();
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
}
