<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;


    protected $fillable = [
        'user_id',
        'title',
        'subtitle',
        'text',
        'likes',
        'views',
        'active',
    ];

    protected static function newFactory()
    {
        return \Modules\Post\Database\factories\PostFactory::new();
    }

    public function images()
    {
        return $this->hasMany(PostImage::class);
    }
}
