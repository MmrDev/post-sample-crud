<?php

namespace App\Repositories;

interface PostRepositoryInterface
{

    public function getAll();

    public function getPost($id);

    public function createPost($data);

    public function UpdatePost($id, $data);

    public function DestroyPost($id);
}
