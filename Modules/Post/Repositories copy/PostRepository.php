<?php

namespace App\Repositories;


use Modules\Post\Entities\Post;

class PostRepository implements PostRepositoryInterface
{
    public function getAll()
    {
        return Post::all();
    }

    public function getPost($id)
    {
        return Post::findOrFail($id);
    }

    public function createPost($data)
    {
        return Post::create($data);
    }

    public function updatePost($id, $data)
    {
        return Post::find($id)->update($data);
    }

    public function destroyPost($id)
    {
        return Post::find($id)->delete();
    }
}
