<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'post'], function () {
    // without Token
    Route::get('', [\Modules\Post\Http\Controllers\Api\PostController::class, 'index']);
    Route::get('{id}', [\Modules\Post\Http\Controllers\Api\PostController::class, 'show']);

    Route::group(['middleware' => 'auth.api'], function () {
        // with Token
        Route::post('', [\Modules\Post\Http\Controllers\Api\PostController::class, 'store'])->middleware('scopes:create');
        Route::post('{id}', [\Modules\Post\Http\Controllers\Api\PostController::class, 'update']);
        Route::delete('{id}', [\Modules\Post\Http\Controllers\Api\PostController::class, 'destroy']);
    });
});
