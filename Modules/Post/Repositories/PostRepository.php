<?php

namespace Modules\Post\Repositories;


use Modules\Post\Entities\Post;
use Modules\Post\Entities\PostImage;

class PostRepository implements PostRepositoryInterface
{
    public function getAll()
    {
        $perPage = Request()->has('perPage') ? Request()->get('perPage') : 10;

        $postCollection = Post::where([
            ['active', '=', true],
        ])->with('images')->paginate($perPage);

        if (count($postCollection)) {
            return $postCollection;
        }
        return false;
    }

    public function getPost($id)
    {
        $postInstance = Post::where([
            ['id', '=', $id],
            ['active', '=', true],
        ])->with('images')->first();
        if ($postInstance instanceof Post) {
            return $postInstance;
        }
        return false;
    }

    public function createPost($data)
    {

        $imagesReq = $data['images'];
        unset($data['images']);

        $data['user_id'] = auth()->id();
        $data['active'] = true;

        $postInstance = Post::create($data)->toArray();


        // save images section
        foreach ($imagesReq as $item) {
            $postInstance['images'][] = PostImage::create([
                'post_id' => $postInstance['id'],
                'url' => uploadFilePro($item, 'post', 'postData')['address'],
                'active' => true
            ]);
        }

        return $postInstance;
    }


    public function updatePost($postId, $data)
    {
        $postInstance = Post::where([
            ['id', '=', $postId],
            ['user_id', '=', auth()->id()],
        ])->with('images')->first();

        if ($postInstance instanceof Post) {



            if (array_key_exists('images', $data)) {
                $images = $data['images'];
                unset($data['images']);

                foreach ($images as $item) {
                    $image = $postInstance->images()->where('id', $item['id']);
                    if ($image instanceof PostImage) {
                        $image->update([
                            'url' => uploadFilePro($item['image'], 'post', 'postData')['address']
                        ]);
                    }
                }
            }

            $postInstance->update($data);

            return $postInstance;
        }
        return false;
    }

    public function destroyPost($id)
    {
        $postInstance = Post::where([
            ['id', '=', $id],
            ['user_id', '=', auth()->id()],
        ])->first();
        if ($postInstance instanceof Post) {
            $postInstance->delete();
            return true;
        }
        return false;
    }
}
